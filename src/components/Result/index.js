


import React from 'react';
import PropTypes from "prop-types";
import './styles.scss';
import {FaListUl} from "react-icons/fa";
import {FaSortAmountDown} from "react-icons/fa";
import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect
  } from "react-router-dom";

class Results extends React.Component {
    constructor(props) {
        super(props)
        this.state = { movies:[], sortNameON:true, sortDateOn:true, galView: "list", curIndex:0,selectedIndex: 0}
        this.sortByName = this.sortByName.bind(this);
        this.sortByDate = this.sortByDate.bind(this);
        this.changeView = this.changeView.bind(this);
        this.getprev = this.getprev.bind(this);
        this.handlePrevNext  = this.handlePrevNext.bind(this);
        this.handlePrev  = this.handlePrev.bind(this);
      // this._TogglePrev = this._TogglePrev.bind(this);
      // this._ToggleNext = this._ToggleNext.bind(this);
        
      }
      // static getDerivedStateFromProps(props, state) {
      //   if (props.movies.length !== state.movies.length) {
      //     return {
      //       movies: props.movies
      //     };
      //   }
      //   return null;
      // }
    sortByName(){
        this.setState(prevState => ({
            sortNameON: !prevState.sortNameON
          }));
        console.log("got here");
        if(this.state.sortNameON){
        let myData = this.props.movies;
        myData.sort(function(a, b){
              var nameA=a.title.toLowerCase(), nameB=b.title.toLowerCase();
              if (nameA < nameB) //sort string ascending
               return -1;
              if (nameA > nameB)
               return 1;
              return 0; //default return value (no sorting)
             });
          this.setState(()=>{return{movies:myData}});
            }else{
                let myData = this.props.movies;
        myData.sort(function(a, b){
              var nameA=a.title.toLowerCase(), nameB=b.title.toLowerCase();
              if (nameA < nameB) //sort string ascending
               return 1;
              if (nameA > nameB)
               return -1;
              return 0; //default return value (no sorting)
             });
          this.setState(()=>{return{movies:myData}});
            }
      }

      sortByDate(){
        this.setState(prevState => ({
            sortDateON: !prevState.sortDateON
          }));
        console.log("got hsere");
        if(this.state.sortDateON){
        let myData = this.props.movies;
        myData.sort(function(a, b){
              var nameA=a.release_date, nameB=b.release_date;
              if (nameA < nameB) //sort string ascending
               return -1;
              if (nameA > nameB)
               return 1;
              return 0; //default return value (no sorting)
             });
          this.setState(()=>{return{movies:myData}});
            }else{
                let myData = this.props.movies;
        myData.sort(function(a, b){
              var nameA=a.release_date, nameB=b.release_date;
              if (nameA < nameB) //sort string ascending
               return 1;
              if (nameA > nameB)
               return -1;
              return 0; //default return value (no sorting)
             });
          this.setState(()=>{return{movies:myData}});
            }
          
      }

    changeView(){
      var css = (this.props.galView === "list") ? "gal" : "list";
      this.setState({galView:css});

    }
    getprev(index){
      // console.log("id" + index);
      // let ss;
      //   if(index ==1){
      //     if (this.state.movies.length > 0) {
          
      //     console.log("len"+ss);
      //     ss= this.state.movies.length-1;
      //     return this.state.movies[ss].id;
      //     }else{
      //       ss = 1;
      //     }
      //   }else{
      //     if (this.state.movies.length > 0) {
      //      ss= index-1;
      //     return this.state.movies[ss].id;
      //     }
      //   }
      //   console.log(ss);
      // const numberOfSlides = renderableRoutes.length - 1;
      //   console.log(numberOfSlides);
    }

    handlePrevNext(index){
        // this.setState({ movies:this.state.movies});

        // const { value } = event.target;
        // console.log(value);

        this.setState(prevState => {
          if(index+1 >= this.props.movies.length-1){
            return { selectedIndex: 0 }
           }
         console.log(this.state.selectedIndex);
          return { selectedIndex: index+1 }
        });
      

    }

    handlePrev(index){
      
      this.setState(prevState => {
          
         if(index-1 <0){
          return { selectedIndex: this.props.movies.length-1 }
         }
        return { selectedIndex: index-1 }
      });
    }

            
//     _ToggleNext(index) {
//       if(this.state.selectedIndex == this.state.movies.length - 1)
//           return;

//       this.setState(() => ({
//           selectedIndex: index + 1
//       }))
// //       if(this.state.selectedIndex == this.props.movies.length - 1)
// //       return;
// //       if(this.props.movies.length>0){
// //         console.log("hre");
// //   this.setState(prevState => ({
// //       selectedIndex: (this.props.movies[0]).id
// //   }))
// // }
//   }

//   _TogglePrev() {
//       // if(this.state.selectedIndex == 0)
//       //  return;

//       // this.setState(prevState => ({
//       //     selectedIndex: prevState.selectedIndex - 1
//       // }))
//             if(this.state.selectedIndex == 0)
//        return;

//       this.setState(prevState => ({
//           selectedIndex: prevState.selectedIndex - 1
//       }))
//   }

  render() {
    return (
     <div>
          <div className="settingContainer">

<div className = 'viewSelect setting'> 
<FaListUl className='icon'/>
<p onClick = {this.changeView}>View</p>
</div>

<div className = 'viewSort setting'> 
<FaSortAmountDown className='icon'/>  
<p onClick={this.sortByName}>Sort by Name</p>

</div>

<div className = 'viewFilter setting'> 
<FaSortAmountDown className='icon'/>
<p onClick={this.sortByDate}>Sort by Date Released</p>
</div>


</div>
 <div className={this.props.galView}>
          {/* <div>{this.props.movies.length}</div> */}
          <Router>
        {this.props.movies.map((movie, index) => (
            
            <div>
                
                <div className='block'>
            
            <div className='poster'>
            {/* <Link to={'/'+movie.id} className="Results-poster"> */}
            <Link to={'/'+index} className="Results-poster">
            <img src ={'https://image.tmdb.org/t/p/w185/'+movie.poster_path} onError={this.addDefImg} width = '200px' alt='poster'></img>
            </Link>
            </div>
            
            {/* <Link to={'/'+movie.id} className="Results-title" > */}
            <Link to={'/'+index} className="Results-title" > 
            <div className='movieInfo'>
              <div className='title'>{movie.title}</div>
              <div>{movie.release_date}<br />{movie.tagline}</div>
              </div>
            </Link>
            
            
            </div>
           
            {/* <Route path={"/"+movie.id}> */}
            <Route path={"/"+index}>
                <div className='detailInfo'>
                  <Link to='/' className='close'>
                <span>x</span>
                </Link>
              <h1 > {movie.title}</h1>
              <div>
                Release Date: {movie.release_date}
              </div>

{/*             
              <ul>
              <div className='genrelist'>Country: 
              {movie.production_countries.map((country)=>(
                <li>{country.name}</li>
              ))}
              </div>
              </ul> */}
              {/* <Content key ={index} componentData ={movie} /> */}
              
              {/* <ul >
              <div className='genrelist'>Genres: 
              {movie.genres.map((genre)=>(
                <li>{genre.name}</li>
              ))}
              </div>
              </ul> */}
              
              
              <div>
                Overview:<br/>
                {movie.overview}
              </div>
              {/* movieid:{
                this.props.movies.length-1>index?(
                  this.props.movies[index+1]
                )
                  } */}
                 {/* <Carousel slides={this.state.movies} ind = {index} /> */}
              
              {/* <a className="prev" onclick="getprev(index)">hhkkk</a> */}
    {/* <a class="next" onclick="plusSlides(1)">&#10095;</a> */}
    {/* <button className="toggle toggle--prev" onClick={this._TogglePrev}>Prev</button>
                    <button className="toggle toggle--next" onClick={this._ToggleNext(index)}>Next</button> */}
                    {/* <Link to={'/'+this.state.selectedIndex} className="prev">&#10094;</Link>
                    <Link to={'/'+this.state.selectedIndex} className="prev" >Next</Link> */}
                    {/* <Next Link to ={'/deff'} onClick={this.handlePrevNext} /> */}
                    <div className='control'>
                    <Link to={'/deff'} onClick={(event)=>this.handlePrev(index, event)} className="prev" >Previous</Link>

                    <Link to={'/deff'}  onClick={(event)=>this.handlePrevNext(index,event)} className="prev">Next</Link>
                    </div>
                    
              </div>
            </Route>
            <Route path={'/deff'}>
              
                    <Redirect to ={'/'+this.state.selectedIndex} />

            </Route>

          </div>
          
          
        
        ))}
        </Router>

       
        
        </div>
        </div>
        ////////end of render function
    );
    
    
  }
  addDefImg(ev){
    ev.target.src = 'https://www.publicdomainpictures.net/pictures/280000/velka/not-found-image-15383864787lu.jpg'
  }


}

Results.propTypes = {
  movies: PropTypes.arrayOf(
    PropTypes.shape({
    //   symbol: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired
      
    })
  ).isRequired
};

Results.defaultProps = {
  movies: []
}

// class Content extends React.Component {
//   render() {
//      return (
//         <div>
//            <ul >
//               <div className='genrelist'>Genres: 
              
//               {/* {this.props.componentData.genres.map((genre)=>(
//                 <li>{genre.name}</li>
//               ))
//               } */}
//               </div>
//               </ul>
//            {/* <div>{this.props.componentData.genres}</div>
//            <div>{this.props.componentData.id}</div> */}
//         </div>
//      );
//   }
// }
export default Results;



