import React from 'react';
import PropTypes from "prop-types";
import './styles.scss';

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  static propTypes = {
    onChange: PropTypes.func
  };

  static defaultProps = {
    onChange: () => {}
  }

  handleChange(event) {
    const { onChange } = this.props;
    onChange(event);
  }

  render() {
    return (
      <div className="Search">
        <input className="Search-input" onChange={this.handleChange} placeholder='Enter Movie Name'/>
      </div>
    );
  }
}

export default Search;