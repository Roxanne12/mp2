import React from 'react';
import PropTypes from "prop-types";
// import './styles.scss';

class GalView extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  static propTypes = {
    onClick: PropTypes.func
  };

  static defaultProps = {
    onClick: () => {}
  }

  handleChange(event) {
    const { onClick } = this.props;
    onClick(event);
  }

  render() {
    return (
      <div className="Search">
       {/* <Filter onClick={this.handlefilterClick} /> */}

       <div className="Search_genre" onClick={this.handleChange}> 
        <input type="button" value="Action"/>
        <input type="button" value="Adventure"/>
        <input type="button"  value="Animation"/>
        <input type="button" value="Comedy"/>
        <input type="button"  value="Crime"/>
        <input type="button" value="Documentary"/>
        <input type="button" value="Drama"/>
        <input type="button" value="Family"/>
        <input type="button" value="Fantasy"/>
        <input type="button"  value="History"/>
        <input type="button"  value="Horror"/>
        <input type="button"  value="Music"/> 
        <input type="button"  value="Mystery"/>
        <input type="button"  value="Romance"/>
        <input type="button" value="Science Fiction"/>
        <input type="button" value="TV Movie"/>

        <input type="button" value="Thriller"/>
        <input type="button"  value="War"/>
        <input type="button" value="Western"/>
        </div>

        
      </div>
    );
  }
}

export default GalView;