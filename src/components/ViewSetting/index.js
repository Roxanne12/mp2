import React from 'react';
import PropTypes from "prop-types";
import './styles.scss';


import {FaListUl} from "react-icons/fa";
import {FaSortAmountDown} from "react-icons/fa";

class ViewSetting extends React.Component{

    constructor(props) {
        super(props)
        this.state = { movies:this.props.movies }
        this.sortByName = this.sortByName.bind(this);
        
      }
      static propTypes = {
        onChange: PropTypes.func
      };
    
      static defaultProps = {
        onChange: () => {}
      }

      sortByName(event) {
    //     this.setState(prevState => {
    //       this.props.movies.sort((a, b) => (b.title - a.title))
    //   });

        const { onClick } = this.props;
        onClick(event);
      
      }
    render(){
        return (
            <div className="settingContainer">

            <div className = 'viewSelect setting'> 
            <FaListUl className='icon'/>
            <p>View</p>
            </div>

            <div className = 'viewSort setting'> 
            <FaSortAmountDown className='icon'/>  
            <div onClick={this.sortByName}>Sort by</div>
            
            </div>

            <div className = 'viewFilter setting'> 
            <FaSortAmountDown className='icon'/>
            <p>Sort by</p>
            </div>

            </div>
            
                );
    }
}

export default ViewSetting;
