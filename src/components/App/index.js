import React from 'react';
import Header from '../Header';
import Search from '../Search';
import axios from 'axios';
import GalView from "../GalView";
import './styles.scss';
import Results from '../Result';
import {
  BrowserRouter as Router,
  Route,
  Link
} from "react-router-dom";


class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = { movies: [], dfmovies: [], view: 'list' }
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handlefilterClick = this.handlefilterClick.bind(this);
    // this.sortByName = this.sortByName.bind(this);
  }

  componentDidMount() {
    //get the latest id
    let newl = [];
    for (let i = 1; i <= 50; i++) {
      axios.get('https://api.themoviedb.org/3/movie/' + i + '?api_key=dc299ada0cbcd158ddf4dd9eb6811c0c')
        .then(response => {
          if (response.data) {

            newl.push(response.data);
            this.setState(() => {
              return { movies: newl, dfmovies: newl }
            });
            // this.setState(() => {
            //   return {dfmovies: newl }
            // });
          }

        })
        .catch(error => {
          console.log(error);
        });

    }
  }
  handlefilterClick(event) {
    // this.setState({ movies:this.state.movies});
    const { value } = event.target;
    if (value !== undefined) {
      let genre_id = 28;
      if (value === "Action") {
        genre_id = 28;
      } 
      else if (value === "Adventure") {
        genre_id = 12;
      } 
      else if (value === "Animation") {
        genre_id = 16;
        
      } 
      else if (value === "Comedy") {
        genre_id = 35;
      } 
      else if (value === "Crime") {
        genre_id = 80;
      
      } 
      else if (value === "Documentary") {
        genre_id = 99;
      } 
      else if (value === "Drama") {
        genre_id = 18;
      } 
      else if (value === "Family") {
        genre_id = 10751;
      } 
      else if (value === "Fantasy") {
        genre_id = 14;
      } 
      else if (value === "History") {
        genre_id = 36;
      } 
      else if (value === "Horror") {
        genre_id = 27;
      } 
      else if (value === "Music") {
        genre_id = 10402;
      } 
      else if (value === "Mystery") {
        genre_id = 9648;
      } 
      else if (value === "Romance") {
        genre_id = 10749;
      } 
      else if (value === "Science Fiction") {
        genre_id = 878;
      } 
      else if (value === "TV Movie") {
        genre_id = 10770;
      } 
      else if (value === "Thriller") {
        genre_id = 53;
      } 
      else if (value === "War") {
        genre_id = 10752;
      } 
      else if (value === "Western") {
        genre_id = 37;
      } 
   
      let temp = [];
    

      for (let i = 1; i <= 3; i++) {
        axios.get('https://api.themoviedb.org/3/discover/movie?api_key=dc299ada0cbcd158ddf4dd9eb6811c0c&page='+i+'&with_genres='+genre_id)
          .then(response => {
            if (response.data) {
              temp.push(...response.data.results);
              this.setState({
                movies: temp,
                // gen: genre,
              })
            }

          })
          .catch(error => {
            console.log(error);
          });
      }
   
      
    }

  }
  handleSearchChange(event) {
    // this.setState({ movies:this.state.movies});
    const { value } = event.target;
    if (value !== "") {
      var snewl = [];
      //   console.log("here"+pag_num);
      for (let k = 1; k <= 5; k++) {
        axios.get('https://api.themoviedb.org/3/search/movie?api_key=dc299ada0cbcd158ddf4dd9eb6811c0c&query=' + value + "&page=" + k)
          .then(response => {
            if (response.data) {
              // filtered.push( response.data );
              // this.setState({ movies:response.data.results});
              snewl.push(...response.data.results);
              this.setState(() => {
                return { movies: snewl }
              });

            }

          })
          .catch(error => {
            console.log(error);
          });

      }
    } else {
      let newl = this.state.dfmovies;
      this.setState(() => { return { movies: newl } });
    }
  }


  render() {
    return (
      <div className="App">
        <Header />

        <Router>
          <div className='changeView_Con'>
          <Link to={'/gallary'}  className='changeView'>Gallary</Link>
          <Link to={'/'} className='changeView'>List</Link>
          </div>


          <Route path={"/gallary"}>

            <GalView onClick={this.handlefilterClick} />
            <Results movies={this.state.movies} galView='gal' key={1} />
          </Route>

          <Route exact path={"/"}>

            <Search onChange={this.handleSearchChange} />
            <Results movies={this.state.movies} galView='list' key={2} />
          </Route>

        </Router>

        {/* <Search onChange={this.handleSearchChange} /> */}
        {/* <Filter onClick={this.handlefilterClick} /> */}
        {/* <ul>
          
        { this.state.movies.map(movie => <li>{movie.title}</li>)}
         </ul> */}
        {/* <ViewSetting onClick={this.sortByName}/> */}

        {/* <Results movies={this.state.movies} /> */}
        {/* <Results movies={this.state.movies}/> */}


      </div>
    );
  }
}

export default App;