import React from 'react';
import PropTypes from "prop-types";
// import './styles.scss';

class Filter extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  static propTypes = {
    onClick: PropTypes.func
  };

  static defaultProps = {
    onClick: () => {}
  }

  handleChange(event) {
    const { onClick } = this.props;
    onClick(event);
  }

  render() {
    return (
      <div className="Search">
            <button className="toggle toggle--prev" onClick={this.handleChange} value="prev">Prev</button>
                    <button className="toggle toggle--next" onClick={this.handleChange} value ='next'>Next</button>
{/* <button className="Search-input" onClick={this.handleChange} value='Japan'>Japan</button>

<button className="Search-input" onClick={this.handleChange} value='United States of America'>USA</button>

<button className="Search-input" onClick={this.handleChange} value='Finland'>Finland</button> */}
      </div>
    );
  }
}

export default Filter;